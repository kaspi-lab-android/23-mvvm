package kz.company.mvvm.api

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kz.company.mvvm.model.Currency
import mvvm.UseCase

class LoadUseCase(
    private val exchangeApiService: ExchangeApiService
) : UseCase<LoadUseCase.Params, Currency>() {
    override val dispatcher: CoroutineDispatcher = Dispatchers.IO

    override suspend fun execute(parameters: Params): Currency =
        exchangeApiService.get()

    data class Params(val url: String)
}