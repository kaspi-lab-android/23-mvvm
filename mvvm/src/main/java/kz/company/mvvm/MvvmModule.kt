package kz.company.mvvm

import kz.company.common.base.InjectionModule
import kz.company.mvvm.api.ExchangeApiService
import kz.company.mvvm.api.LoadUseCase
import kz.company.mvvm.ui.SampleViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object MvvmModule : InjectionModule {
    override fun create(): Module = module {
        viewModel { SampleViewModel(get()) }
        single { ExchangeApiService.create() }
        single { LoadUseCase(get()) }
    }
}