package kz.company.mvvm.ui

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kz.company.mvvm.api.LoadUseCase
import kz.company.mvvm.model.Currency
import mvvm.BaseViewModel
import mvvm.SingleLiveEvent
import mvvm.launchSafe

class SampleViewModel(
    private val loadUseCase: LoadUseCase
) : BaseViewModel() {

    private val _currency = MutableLiveData<Currency>()
    val currency: LiveData<Currency> = _currency

    private val _actions = SingleLiveEvent<SampleAction>()
    val actions: LiveData<SampleAction> = _actions

    fun load() {
        launchSafe(
            body = {
                val result = loadUseCase(LoadUseCase.Params("www"))
                _currency.postValue(result)
                _actions.postValue(SampleAction.ShowLoading(false))
            },
            handleError = {
                Log.e(it.message, "Error")
                _actions.postValue(SampleAction.ShowLoading(false))
            },
            start = {
                _actions.postValue(SampleAction.ShowLoading(true))
            },
            finish = {
                // Скрываю загрузку
            }
        )
    }

    sealed class SampleAction{
        data class ShowLoading(val isVisible: Boolean): SampleAction()
    }
}