package kz.company.mvvm.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_mvvm.*
import kz.company.mvvm.R
import mvvm.observeNotNull
import org.koin.androidx.viewmodel.ext.android.viewModel

class SampleMVVMFragment: Fragment(R.layout.fragment_mvvm) {
    private val viewModel: SampleViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.load()
        viewModel.currency.observeNotNull(viewLifecycleOwner){ currency ->
            textView.text = currency.rates.toString()
        }
        viewModel.actions.observeNotNull(viewLifecycleOwner){
            when(it){
                is SampleViewModel.SampleAction.ShowLoading -> {
                    it.isVisible
                    // Покажи / скрой
                }
            }
        }
    }
}