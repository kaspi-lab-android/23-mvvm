package kz.company.mvp.model

data class Currency(
    val rates: Map<String, Double>,
    val base: String,
    val date: String
)