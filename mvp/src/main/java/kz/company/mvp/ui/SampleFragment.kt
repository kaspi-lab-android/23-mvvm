package kz.company.mvp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_sample.*
import kz.company.common.base.BaseFragment
import kz.company.mvp.R
import kz.company.mvp.model.Currency
import org.koin.androidx.viewmodel.ext.android.viewModel

class SampleFragment :
    BaseFragment<SampleContract.View, SampleContract.Presenter>(),
    SampleContract.View {

    override val presenter: SamplePresenter by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        inflater.inflate(R.layout.fragment_sample, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.load()
    }

    override fun show(currency: Currency) {
        textView.text = currency.rates.toString()
    }

    override fun showInfo() {

    }
}